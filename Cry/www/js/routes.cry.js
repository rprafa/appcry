/* This is the routes to cry */

(function(){
  'use strict';
  
  angular
    .module('cry')
    .config(ApplicationConfig);

    //set dependencies for ApplicationConfig
    ApplicationConfig.$inject = ['$ionicConfigProvider', '$stateProvider', '$urlRouterProvider'];
    
    function ApplicationConfig($ionicConfigProvider, $stateProvider, $urlRouterProvider){
      //define routes
      $stateProvider
        .state('main', {
          url: '/main',
          templateUrl: 'main/main.html',
          controller: 'MainCtrl',
          controllerAs: 'main'
        });

      //set default state
      $urlRouterProvider.otherwise('/main');
    }

})();