/* Este é o controlador da view "main" */

(function(){
  'use strict';

  angular
    .module('main')
    .controller('MainCtrl', MainCtrl);

    // Atribuindo as dependências de MainCtrl
    MainCtrl.$inject = ['$ionicPlatform', '$ionicPopup', '$cordovaNativeAudio', '$scope'];
    
    // Controller da view "main"
    function MainCtrl($ionicPlatform, $ionicPopup, $cordovaNativeAudio, $scope) {
      console.log('MainCtrl');
      /* jshint validthis: true */
      var vm = this;
      var id = 0;
      vm.cry = 0;

      // Assim que a plataforma estiver pronta ("ready"), fazer o pré-carregamento
      // do arquivo de áudio do beep
      $ionicPlatform.ready(function() {

          $cordovaNativeAudio.preloadSimple('beep', 'audio/Censored_Beep-Mastercard.mp3')
                   .then(function(msg) { console.log(msg); })
                   .catch(function(error) { console.error(error); });
      });

      // Executa a função vibrate automaticamente
      vibrate();

      /** FUNÇÕES **/

      // Adiciona um listener no valor remoto de "cry" e controla a vibração e sons
      function vibrate() {
      	firebase.database().ref("/").on('value', function(snapshot) {
          // Se o valor de "cry" for 1, o dispositivo deve vibrar e fazer barulho
        	if (snapshot.val().cry == 1) {
            // Atribui o valor 1 para o "cry" local
        		vm.cry = 1;

            // Iniciar o timer
        		$scope.$broadcast('timer-start');

            // Necessário armazenar o intervalo do beep para, futuramente, interrompê-lo
            id = setInterval(function(){
                        $cordovaNativeAudio.play('beep')
                          .then(function(msg) { console.log(msg); })
                          .catch(function(error) { console.error(error); });
                     }, 1000);

            // Ativar a vibração por 99999999 ms, ou aproximadamente 28 dias
            // Este plugin não funciona corretamente no iOS
      			navigator.vibrate(99999999);
      		}
          // Se o valor de "cry" for 0, é hora de parar a vibração e o barulho
      		else if (snapshot.val().cry == 0) {
            // Atribui o valor 0 para o "cry" local
      			vm.cry = 0;
            // Para o timer na contagem atual
    			  $scope.$broadcast('timer-stop');
            // Interrompe on intervalo do beep, silenciando o barulho
            clearInterval(id);
            // Interrompe a vibração
      			navigator.vibrate(0);
      		}
      	});
      }

      // Função para desativação (ou parada, ou interrupção) das vibrações e barulhos
      vm.deactivate = function() {
        // Se o "cry" local tiver valor 1, trocá-lo para 0 e atualizar o "cry" remoto para refletir esta mudança
      	if (vm.cry == 1) {
      		firebase.database().ref("/").update({cry: 0}).then(

            // Se obtiver sucesso, para o timer e muda o valor local de "cry" para 0 (parando a vibração)
            function() {
              vm.cry = 0;
              $scope.$broadcast('timer-stop');
            },

            // Se falhar, mostra um popup com a mensagem de erro e 
            // para a vibração (marcando "cry" como 0 e parando o timer)
            function() {
              $ionicPopup.alert({
                title: 'Erro',
                template: 'Falha na conexão com o banco de dados.',
                okType: 'button-balanced'
              });
              vm.cry = 0;
              $scope.$broadcast('timer-stop');
            }
          );
        }
      }
  	}

})();